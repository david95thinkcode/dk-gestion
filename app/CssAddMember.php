<?php 
namespace App;

use DB;
use App\User;
use App\UpgradeRewardTwo;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Controller\Route;


class CssAddMember {
    
    public $var;
    
    public function __construct(Request $request){
        $this->var = $request;
    }
    
    public function addHim(){
        
        $parr = User::where('pseudo', $this->var->pseudo_parrain)->first();
        $pId = $parr->id;

        $userAdd = new User;
        
        $userAdd->pseudo = $this->var->pseudo;
        $userAdd->first_name = $this->var->first_name;
        $userAdd->last_name = $this->var->last_name;
        $userAdd->email = $this->var->email;
        $userAdd->pays = $this->var->pays;
        $userAdd->no_compte = $this->var->no_compte;
        $userAdd->banque_id = $this->var->banque_id;
        $userAdd->password = bcrypt($this->var->password);
        $userAdd->mdp_transact = $this->var->mdp_transact;
        $userAdd->parrain_id = $pId;
        $userAdd->save();

        $newMan = User::where('pseudo', $this->var->pseudo)->first();
        
        $grade = new UpgradeRewardTwo($newMan->id);
            $grade->upgradeMember(); 
        
    }
    
}