<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Http\Controller\Route;
use Illuminate\Support\Str;
use DB;
use App\User;
use App\Upgrade;

class UpgradeRewardTwo
{
    public  $funderCode = 1;
    public $codeMember;
    public $levels = array('Membre', 'Niveau 1', 'Niveau 2', 'Niveau 3',
    'Niveau 4', 'Niveau 5', 'Niveau 6', 'Diamant');
    public $rewards = array(0, 5000, 10000, 20000, 50000, 300000, 2000000, 10000000 );
    
    function __construct($code){
        
        $this->codeMember = $code;
        
    }
    
    public function makeTheLine(){
        $line = [];
        
        $passingCode = $this->codeMember;
        
        $line[0] = $this->codeMember;
        
        $incre = 1;
        
        while($passingCode != 1){
            
            $passingCode = DB::table('users')->where('id', '=', $passingCode)->get()[0]->parrain_id;
            
            $line[$incre] = $passingCode;
            
            $incre += 1;
        }
        
        return $line;
        
    }
    
    public function checkUpgradeAvailable($hisCode){
        
        $current = DB::table('users')->where('id', '=', $hisCode)->get()[0]->level;
        
        if($current == "Membre"){
            
            return DB::table('users')->where('parrain_id', '=', $hisCode)->get()->count() >= 2;
        }
        
        
        if ( DB::table('users')->where('parrain_id', '=', $hisCode)->get()->count() < 2)
        return false;
        
        $downLineOne = DB::table('users')->where('parrain_id', '=', $hisCode)->get();
        
        $kid1 = $downLineOne[0]->id;
        $kid2 = $downLineOne[1]->id;
        
        $downLineTwo = DB::table('users')->where('parrain_id', '=', $kid1)->get();
        
        if(count($downLineTwo) < 2) return false;
        
        $downLineThree = DB::table('users')->where('parrain_id', '=', $kid2)->get();
        
        if(count($downLineThree) < 2) return false;
        
        if($current != $downLineOne[0]->level)return false;
        if($current != $downLineOne[1]->level)return false;
        if($current != $downLineTwo[0]->level)return false;
        if($current != $downLineTwo[1]->level)return false;
        if($current != $downLineThree[0]->level)return false;
        if($current != $downLineThree[1]->level)return false;

        
        return true;
        
    }
    
    public function upgradeMember(){
        
        $theList = $this->makeTheLine();
        
        for ($i=0; $i < count($theList); $i++) { 
            
            $value = $theList[$i];

            if($this->checkUpgradeAvailable($value)){
                
                $hisLevel = DB::table('users')->where('id', '=', $value )->get()[0]->level;
                $hisId = DB::table('users')->where('id', '=', $value )->get()[0]->id;
                
                $index = 0;
                
                for ($i=0; $i < 8; $i++) { 
                    if($this->levels[$i] == $hisLevel){
                        $index = $i;
                        break;
                    }
                }
                
                $toLevel =  $this->levels[$index + 1];
                
                $upgrade = new Upgrade;
                
                $upgrade->code = $value;
                $upgrade->from = $hisLevel;
                $upgrade->to = $toLevel;
                $upgrade->bonus = $this->rewards[$index + 1];
                
                $upgrade->save();
                
                $upgradeMember = User::find($hisId);
                
                $upgradeMember->level = $toLevel;
                
                $upgradeMember->save();
            }
        }
        
        
    }
}


