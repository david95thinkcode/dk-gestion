<?php

namespace App;

use App\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MakeGenealogie 
{
    public $rootCode;
    
    public function __construct(string $code){
        $this->rootCode = $code;
    }
    
    public function hasKids(string $code){
        return count(Members::where('codeP', '=', $code)->get());
    }
    
    public function treeView(){   
        
        $Categorys = Members::where('codeP', '=', $this->rootCode)->get();
        
        $tree='<ul id="browser" class="filetree"><li class="tree-view"></li>';
        
        foreach ($Categorys as $Category) {
            
            $label = $Category->code . ' ' . $Category->nom . ' ' . $Category->prenom . ' ' . $Category->level;
            
            $tree .='<li class="tree-view closed"<a class="tree-name">'. $label . '</a>';
                       
            if($this->hasKids($Category->code)) {
                
                $tree .=$this->childView($Category);
            }
        }
        $tree .='<ul>';
        return $tree;
        // return view('proofs.gene',compact('tree'));
    }
    public function childView($Category){     
        
        $html ='<ul>';
        
        $kids = DB::table('members')->where('codeP', '=', $Category->code)->get();
        
        foreach ($kids as $arr) {
            
            if($this->hasKids($arr->code)){
                
                $label = $arr->code . ' ' . $arr->nom . ' ' . $arr->prenom . ' ' . $arr->level;
                
                $html .='<li class="tree-view closed"><a class="tree-name">'.$label.'</a>';  
                
                $html.= $this->childView($arr);
                
            }else{
                $label = $arr->code . ' ' . $arr->nom . ' ' . $arr->prenom . ' ' . $arr->level;
                $html .='<li class="tree-view"><a class="tree-name">'.$label.'</a>';                                 
                $html .="</li>";
            }
            
        }
        
        $html .="</ul>";
        return $html;
    }
}
