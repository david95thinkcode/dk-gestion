<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompteSolde extends Model
{
     protected $fillable = ['date_solde', 'user_id', 'montant',
   	];

   	public function user(){
   		return $this->belongsTo(App\User);
   	}
}
