<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $fillable = ['date_bonus', 'user_id', 'justification', 'mont_bonus',
   	];

   	public function user(){
   		return $this->belongsTo(App\User);
   	}
}
