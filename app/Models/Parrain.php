<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parrain extends Model
{
    protected $fillable = ['pseudo_fils', 'parrain_id',
   	];

   	public function parent()
    {
        return $this->belongsTo('App\Models\Parrain', 'parent_id');
    }
   	public function childs() {
           return $this->hasMany('App\Models\Parrain','parrain_id','id') ;
   }
}
