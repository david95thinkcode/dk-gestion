<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfert extends Model
{
    protected $fillable = ['date_transf', 'user_id', 'motif', 'mont_transf', 'receveur',
   	];

   	public function user(){
   		return $this->belongsTo('App\User');
   	}
}
