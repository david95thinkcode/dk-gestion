<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompteCode extends Model
{
    protected $fillable = ['date_solde', 'user_id', 'nbr_code',
   	];

   	public function user(){
   		return $this->belongsTo(App\User);
   	}
}
