<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaiementInscription extends Model
{
    protected $fillable = ['date_ins', 'user_id', 'nom_payeur',
   	];

   	public function user(){
   		return $this->belongsTo(App\User);
   	}
}
