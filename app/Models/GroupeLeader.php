<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupeLeader extends Model
{
    protected $fillable = ['nom_group', 'pays', 'responsable', 'contact',
   	];
}
