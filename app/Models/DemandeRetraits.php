<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemandeRetraits extends Model
{
   protected $fillable = ['date_dmde', 'user_id', 'mont_dmde', 'groupe_id',
   	];
   	public function user(){
        return $this->belongsTo('App\User');
    }
}
