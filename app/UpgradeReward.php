<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Http\Controller\Route;
use Illuminate\Support\Str;
use DB;
use App\Members;
use App\Upgrade;

class UpgradeReward 
{
    public  $funderCode = 'a1015';
    public $codeMember;
    public $levels = array('Membre', 'Debutant', 'Niveau1', 'Niveau2', 'Niveau3',
    'Niveau4', 'Niveau5', 'Niveau6', 'Diamant');
    public $rewards = array(0, 5000, 10000, 20000, 50000, 300000, 2000000, 10000000, 50000000 );
    
    function __construct(string $code){
        
        $this->codeMember = $code;
        
    }
    
    public function makeTheLine(){
        $line = [];
        
        $passingCode = $this->codeMember;
        
        $line[0] = $this->codeMember;
        
        $incre = 1;
        
        while($passingCode != 'a1015'){
            
            $passingCode = DB::table('members')->where('code', '=', $passingCode)->get()[0]->codeP;
            
            $line[$incre] = $passingCode;
            
            $incre += 1;
        }
        
        return $line;
        
    }
    
    public function checkUpgradeAvailable($hisCode){
        
        $current = DB::table('members')->where('code', '=', $hisCode)->get()[0]->level;
        
        if($current == "membre"){
            
            return DB::table('members')->where('codeP', '=', $hisCode)->get()->count() >= 2;
        }
        
        
        if ( DB::table('members')->where('codeP', '=', $hisCode)->get()->count() < 2)
        return false;
        
        $downLineOne = DB::table('members')->where('codeP', '=', $hisCode)->get();
        
        $kid1 = $downLineOne[0]->code;
        $kid2 = $downLineOne[1]->code;
        
        $downLineTwo = DB::table('members')->where('codeP', '=', $kid1)->get();
        
        if(count($downLineTwo) < 2) return false;
        
        $downLineThree = DB::table('members')->where('codeP', '=', $kid2)->get();
        
        if(count($downLineThree) < 2) return false;
        
        if($current != $downLineOne[0]->level)return false;
        if($current != $downLineOne[1]->level)return false;
        if($current != $downLineTwo[0]->level)return false;
        if($current != $downLineTwo[1]->level)return false;
        if($current != $downLineThree[0]->level)return false;
        if($current != $downLineThree[1]->level)return false;

        
        return true;
        
    }
    
    public function upgradeMember(){
        
        $theList = $this->makeTheLine();
        
        for ($i=0; $i < count($theList); $i++) { 
            
            $value = $theList[$i];

            if($this->checkUpgradeAvailable($value)){
                
                $hisLevel = DB::table('members')->where('code', '=', $value )->get()[0]->level;
                $hisId = DB::table('members')->where('code', '=', $value )->get()[0]->id;
                
                $index = 0;
                
                for ($i=0; $i < 8; $i++) { 
                    if($this->levels[$i] == $hisLevel){
                        $index = $i;
                        break;
                    }
                }
                
                $toLevel =  $this->levels[$index + 1];
                
                $upgrade = new Upgrade;
                
                $upgrade->code = $value;
                $upgrade->from = $hisLevel;
                $upgrade->to = $toLevel;
                $upgrade->bonus = $this->rewards[$index + 1];
                
                $upgrade->save();
                
                $upgradeMember = Members::find($hisId);
                
                $upgradeMember->level = $toLevel;
                
                $upgradeMember->save();
            }
        }
        
        
    }
}


