<?php

namespace App;

use App\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MakeGenealogieTwo
{
    public $rootCode;
    public $rootName;
    public $treated = ' ';
    public $levColors = array(
        'Membre' => '#FFFFFF',
        'Niveau 1' => '#F1C40F',
        'Niveau 2' => '#E74C3C',
        'Niveau 3' => '#C0392B',
        'Niveau 4' => '#27AE60',
        'Niveau 5' => '#3498DB',
        'Niveau 6' => '#95A5A6',
        'Diamant' => '#1ABC9C');
        
        
        public function __construct(string $code){
            $this->rootCode = $code;
            
            $master = User::where('id', '=', $this->rootCode)->first();
            
            $this->rootName = $master->pseudo;
        }
        
        public function hasKids(string $code){
            
            return count(User::where('parrain_id', '=', $code)->get());
        }
        
        public function makeNode(string $id){
            
            $userKid = User::where('id', '=', $id)->first();
            
            
            $name = $this->getName($userKid->id);
            $parentId = $userKid->parrain_id;
            $parentName = $this->getName($userKid->parrain_id);
            $color  = $this->levColors[$userKid->level];
            $level = $userKid->level;
            
            $nodeText =  '[{v:\''. $name . '\', f:\''. $name . 
                '<div style="background-color:'. $color . ';">'. $level . '</div>\'},
                \''. $parentName . '\', \'\'],';
                
                return $nodeText;
            }
            
            public function getName($id){
                
                $pr = User::where('id', '=', $id)->first();
                
                if($id == NULL)
                return '';
                
                return $pr->pseudo;
            }
            
            public function treeView(){   
                
                // $Categorys = User::where('parrain_id', '=', $this->rootCode)->get();
                
                $lines = '[ ';
                
                $master = User::where('id', '=', $this->rootCode)->first();
                
                $lines .= $this->makeNode($this->rootCode);
                
                if($this->hasKids($master->id)) {
                    
                    $lines .=$this->childView($master);
                }
                
                $lines .= ']';
                
                $fn = str_replace(',]', ' ]', $lines);
                
                return $fn;
                
            }
            public function childView($Category){     
                
                $kids = DB::table('users')->where('parrain_id', '=', $Category->id)->get();
                
                $inline = '';
                
                foreach ($kids as $arr) {
                    
                    $this->treated .= ' ' . $arr->id . '';
                    
                    if($this->hasKids($arr->id)){
                        
                        $inline .= $this->makeNode($arr->id);
                        
                        $inline .= $this->childView($arr);                 
                        
                    }else{
                        
                        $line = $this->makeNode($arr->id);
                        
                        $inline .= $line;
                        
                    }
                }
                
                return $inline;
            }
        }
