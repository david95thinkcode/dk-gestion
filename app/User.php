<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Bonus;
use App\Models\DemandeRetraits;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'pseudo', 'first_name', 'last_name', 'email', 'pays', 'mdp_transact', 'no_compte', 'banque_id', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function bonus(){
        return $this->hasMany('App\Models\Bonus');
    }
     public function demandes(){
        return $this->hasMany('App\Models\DemandeRetraits');
    }
}
