<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Banque;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
    	return view('backend.profile.profile');
    }

    public function create($var){
    	$all_banques = Banque::all();
        $user = Auth::user();
    	return view('backend.profile.update_profil',compact('all_banques', 'user'));
    }

     public function modif_profil(Request $req, $var){

        $this->validate($req,[
            'no_compte' => 'required|string|max:255',
            'banque_id' => 'required|integer',
            'email' => 'required|email|max:255',
        ]);
        
        $mdp = Auth::user()->mdp_transact;
        $prenom =   Auth::user()->firstname;
        $nom =   Auth::user()->lastname;
        $pseudo =   Auth::user()->pseudo;
        $updateinfos=User::whereid(Auth::user()->id)->update([
            'firstname'=>$prenom,
            'lastname'=>$nom,
            'pseudo'=>$pseudo,
            'email'=>$req->email,
            'no_compte'=>$req->no_compte,
            'banque_id'=>$req->banque_id,
        ]);
    }

    public function modif_pwd($id){
       	return view('backend.profile.update_transactions');
    }

    public function update_transact(){


    }
    /* public function update(Request $request, $id)
    {
        $this->validate($request,[
            'mdp_ancien' => 'required',    
            'password' => 'required|string|min:8|confirmed',
            ]  
        );
        // Validate the new password length...

        $user = User::find(Auth::id());
        $hashedPwd = $user->password;

        $request->user()->fill([
            'password' => Hash::make($request->newPassword)
        ])->save();
    }
    public function updatepassword(Request $request, User $user)
    {
         $data = $request->validate([
            'mdp_ancien' => 'required',
            'mdp_transact' => 'required|string|unique:users',
        ]);

        $user->fill($data);
        $user->save();      


        /*$user = User::find(Auth::id());
        $hashedPwd = $user->mdp_transact;
        dd($hashedPwd);
 
        if ($request->mdp_ancien == $hashedPwd) {
            //Change the password
            $user->fill([
                'mdp_transact' => $request->mdp_transact
            ])->save();
 
            return view('backend.profile.profile');
        }
    }
   /* public function store(Request $req, $id){
    	 $this->validate($request,[
            'firstname' => 'required|string|min:4|max:255',
            'lastname' => 'required|string|min:4|max:255',
            'email' => 'required|email|min:10|max:255',
            'mobile' => 'required|integer|min:10|max:255',
            'profession' => 'required|string|min:10|max:255',
            'lieu_profession' => 'required|string|min:10|max:255',
        ]);
        
        $updateinfos=User::whereid(Auth::user()->id)->update([
            'email'=>$request->email,
            'profession'=>$request->profession,
            'lieu_profession'=>$request->lieu_profession,
        ]);
        $users=User::whereid(Auth::user()->id)->get();
        $listPays = Pays::get();
        $users=User::whereid(Auth::user()->id)->get();
        $userspays=Pays::whereid(Auth::user()->pay_id)->get();
        Flashy::success('Informations Modifié avec succès');
        return view('userinfos',compact('users','listPays','users','userspays'));
        
    }*/
}
