<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Controller\Route;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Validator;
use Illuminate\Support\Str;
use DB;
use App\Members;
use App\UpgradeReward;


class MembersController extends Controller
{
    
    public function insertform() {
        return view('proofs/tables');
    }
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        
        //Existing Check
        
        $godFatherExists = DB::table('members')->where('code', $request->input('cc-codeP'))->count();
        
        if($godFatherExists != 1){
            return redirect()->back()->WithInput(Input::all())->withErrors('Le parrain '. $request->input('cc-codeP') . ' nexiste pas !');
        }
        
        //Max Childs Check
        $maxChilds = DB::table('members')->where('codeP', $request->input('cc-codeP'))->count();
        
        if($maxChilds >= 2){
            return redirect()->back()->WithInput(Input::all())->withErrors('Le parrain '. $request->input('cc-codeP') . ' a deja deux filleuls');
        }
        
        $newCode = DB::table('members')->max('id');
        
        $newCode = (int) $newCode;
        
        $newCode += 1;
        
        // while(Str::length($newCode) < 6) $newCode = '0' . $newCode;
        
        $newCode = 'ETF_' . $newCode;
        
        $member = new Members;
        
        $member->nom = $request->input('cc-nom');
        $member->prenom = $request->input('cc-prenom');
        $member->email = $request->input('cc-email');
        $member->codeP = $request->input('cc-codeP');
        $member->code = $newCode;
        $member->password = $request->input('cc-pass');
        $member->tel = $request->input('cc-tel');
        $member->address = $request->input('cc-addr');
        $member->country = $request->input('cc-pays');
        
        if($member->save()){
            echo "Done !<br/>"; 
            
            $grade = new UpgradeReward($newCode);
            $grade->upgradeMember(); 
            
            return redirect('proof');
        }
        
        else
        echo $member->error_get_last();
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $member =  Members::find($request->id);
        
        $member->nom = $request->input('cc-nom');
        $member->prenom = $request->input('cc-prenom');
        $member->email = $request->input('cc-email');
        $member->codeP = $request->input('cc-codeP');
        $member->code = 'def';
        $member->password = $request->input('cc-pass');
        $member->tel = $request->input('cc-tel');
        $member->address = $request->input('cc-addr');
        $member->country = $request->input('cc-pays');
        
        if($member->save){
            echo "Done !<br/>";
        }
        
        else
        echo $member->error_get_last();
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
}
