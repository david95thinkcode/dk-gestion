<?php

namespace App\Http\Controllers;

use App\User;
use App\CssAddMember;
use App\Models\Banque;
use App\Models\Parrain;
use App\Models\CompteCode;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PaiementInscription;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\StoreInscriptionRequest;
use Illuminate\Support\Facades\Redirect;

class InscriptionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$code = DB::table('compte_codes')
        ->where('user_id', Auth::user()->id)
        ->orderBy('id', 'desc')->first();
        $code = CompteCode::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        $nb_code = $code->nbr_code;
        dd($nb_code);*/
        $all_banques = Banque::all();
        return view('auth.inscription', compact('all_banques'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInscriptionRequest $request)
    {
        //Existing Check
        $godFatherExists = User::where('pseudo', $request->pseudo_parrain)->count();

        if ($godFatherExists != 1) {
            $errorMessage = 'Le parrain ' . $request->pseudo_parrain . ' nexiste pas !';
            \Session::flash("errorMessage", $errorMessage);
            return redirect()->back()->WithInput(Input::all())->withErrors($errorMessage);
        }

        $parrain = User::where('pseudo', $request->pseudo_parrain)->first();

        // Control payeur existant
        $payeur = User::where('pseudo', $request->nom_payeur)->first();
        if ($payeur == null) {
            $errorMessage = 'Le payeur ' . $request->nom_payeur . " n'existe pas !";
            \Session::flash("errorMessage", $errorMessage);
            return redirect()->back()->WithInput(Input::all())->withErrors($errorMessage);
        }

        // Control nombre de code payeur
        if ($payeur->nbr_code <= 0) {
            $errorMessage = 'Le payeur ' . $request->nom_payeur . " n'a pas assez de code  !";
            \Session::flash("errorMessage", $errorMessage);
            return redirect()->back()->WithInput(Input::all())->withErrors($errorMessage);
        }

        //Max Childs Check
        $maxChilds = User::where('parrain_id', $parrain->id)->count();
        if ($maxChilds >= 2) {
            $errorMessage = 'Le parrain ' . $request->pseudo_parrain . ' a deja deux filleuls';
            \Session::flash("errorMessage", $errorMessage);
            return redirect()->back()->WithInput(Input::all())->withErrors($errorMessage);
        }

        $custom = new CssAddMember($request);

        $custom->addHim();

        $kid = User::where('pseudo', $request->pseudo)->first();

        $paiement = PaiementInscription::create([
            'date_ins' => date("Y-m-d H:i:s"),
            'user_id' => $kid->id,
            'nom_payeur' => $payeur->pseudo
        ]);

        // Update payeur number of code
        $payeur->nbr_code -= 1;
        $payeur->save();

        $transaction = Transaction::create([
            'date_transact' => date("Y-m-d H:i:s"),
            'sender' => $payeur->pseudo,
            'recever' => $request->pseudo,
            'motif' => "Paiement inscription",
            'mont_transact' => 1000,
        ]);

        return redirect('/home');
    }

    public function manageCategory()
    {
        $categories = Parrain::where('parrain_id', '=', 0)->get();
        $allCategories = Parrain::all();
        $allCategories = $allCategories->toArray();
                    //dd($allCategories);
        return json_encode($allCategories);
                    //return view('auth.arbre', compact('categories','allCategories'));
    }

    public function addParrain(Request $request)
    {
        $this->validate($request, [
            'pseudo_fils' => 'required',
        ]);
        $input = $request->all();
        $input['parrain_id'] = empty($input['parrain_id']) ? 0 : $input['parrain_id'];

        Parrain::create($input);
                        /*Parrain::create([
                            'pseudo_fils' => $request->pseudo_fils,
                            'parrain_id' => $request->parrain_id,
                            ]);*/
        return back()->with('success', 'New Category added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
                            //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                            //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                            //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                            //
    }
}
