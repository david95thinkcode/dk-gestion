<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\CompteCode;
use App\Models\Transaction;
use App\Models\Generer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RechargeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        return view('backend.admin.recharge_code');
    }

    public function store(Request $req)
    {
    	$id = Auth::user()->id;
        $user= User::where('pseudo', $req->pseudo)->first();
        $user_id = $user->id;
    	$sender = Auth::user()->pseudo;
        $code = User::where('id', $id)->first();
        $nb_code = $code->nbr_code;
        $code_now = $nb_code - ($req->nbr_code);

        $solde_code = $user->nbr_code;
        $new = $solde_code + ($req->nbr_code);

        $montant = ($req->nbr_code)*1000;
        
        if($nb_code >= $req->nbr_code){
                Transaction::create([
                    'date_transact' => date("Y-m-d H:i:s"),
                    'sender' => $sender,
                    'recever' => $req->pseudo,
                    'motif' => "Recharge de compte",
                    'mont_transact' => $montant,
                ]);


                DB::table('users')
                        ->where('id', $id)
                        ->update(['nbr_code' => $code_now]);

                DB::table('users')
                        ->where('id', $user_id)
                        ->update(['nbr_code' => $new]);

                Transaction::create([
                    'date_transact' => date("Y-m-d H:i:s"),
                    'sender' => Auth::user()->pseudo,
                    'recever' => $req->pseudo,
                    'motif' => "Recharge de code pour un utilisateur",
                    'mont_transact' => $req->nbr_code,
                ]);

            }


       /* CompteCode::create([
            'date_solde' => date("Y-m-d H:i:s"),
            'user_id' => $user_id,
            'nbr_code' => ($req->nbr_code),
        ]);*/
    }

    public function affich()
    {
        return view('backend.admin.generer');
    }

    public function generer(Request $req){
        Generer::create([
            'date_gene' => date("Y-m-d H:i:s"),
            'nbr_gene' => $req->nbr_code,
            'montant' => $req->nbr_code * 1000,
        ]);

        $id = Auth::user()->id;
        $code = Auth::user()->nbr_code;
        $code_actu = $code + ($req->nbr_code);
        DB::table('users')
            ->where('id', $id)
            ->update(['nbr_code' => $code_actu]);
            return redirect()->route('home');
    }

}
