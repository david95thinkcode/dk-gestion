<?php

namespace App\Http\Controllers;

use App\User;
use App\Upgrade;

use App\Models\Bonus;
use App\Models\CompteCode;
use App\Models\CompteSolde;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

   
    public function index()
    {
        $transactions = Transaction::all();
        $code = Auth::user()->nbr_code;
        $solde = Auth::user()->solde;
        $kids = count(User::where('parrain_id', Auth::user()->id)->get());
        $bonus = Upgrade::where('code', Auth::user()->id)->sum('bonus');
        $lev = Auth::user()->level;
       

       
        return view('backend.dashboard', compact('transactions', 'code', 
        'solde', 'kids', 'bonus', 'lev'));
    }
    public function logout()
    {
        auth()->logout();

        session()->flash('message', 'Some goodbye message');

        return redirect('/login');
    }
}
