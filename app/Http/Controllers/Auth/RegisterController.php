<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Banque;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

     

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showRegistrationForm()
    {
        $all_banques = Banque::all();
        return view('auth.inscription', compact('all_banques'));
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'pseudo' => 'required|string|max:8|unique:users',
            'email' => 'required|string|email|max:255',
/*            'tel' => 'required|string|max:255|unique:users',
*/            'pays' => 'required|string|max:50',
            'mdp_transact' => 'required|string|max:20|unique:users',
            'no_compte' => 'required|string|max:25',
            'banque_id' => 'required|integer',
            'password' => 'required|string|min:6|confirmed',
            'nom_payeur' => 'required|integer',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'pseudo' => $data['pseudo'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'pays' => $data['pays'],
            'mdp_transact' => $data['mdp_transact'],
            'no_compte' => $data['no_compte'],
            'banque_id' => $data['banque_id'],
            'password' => bcrypt($data['password']),
        ]);

        $user_id = User::last()->get();
        
    }
}
