<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupeLeader;
use App\Models\DemandeRetraits;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DemandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i = 1;
        $demandes = DemandeRetraits::where('valide', 0)->get();
        
        /*$pays = $demandes->pluck('pays');
        $pseudo = $demandes->pluck('pseudo');*/
        //dd($demandes);
        return view('backend.admin.list_demande', compact('demandes', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $verif = $user->pays;
        $groupes = GroupeLeader::where('pays', $verif)->get();
        return view('backend.demande_retrait', compact('groupes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solde = Auth::user()->solde;
        if ($solde >= $request->mont_dmde) 
        {
            DemandeRetraits::create([
                'date_dmde' => date("Y-m-d H:i:s"),
                'user_id' => Auth::user()->id,
                'mont_dmde' => $request->mont_dmde,
                'groupe_id' => $request->group_id,
            ]);

            $code = User::where('id', Auth::user()->id)->first();
            $nb_code = $code->solde;
            $new = $nb_code - ($request->mont_dmde);
                
            DB::table('users')
                ->where('id', Auth::user()->id)
                ->update(['solde' => $new]);
        }
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
