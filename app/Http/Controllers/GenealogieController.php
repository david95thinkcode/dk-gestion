<?php

namespace App\Http\Controllers;

use App\Members;
use Illuminate\Http\Request;

class GenealogieController extends Controller
{
    public $rootCode;
    
    function __construct(string $code){
        $this->rootCode = $code;
    }
    
    public function treeView(){   
        
        $Categorys = Members::where('codeP', '=', $this->rootCode)->get();
        
        
        foreach ($Categorys as $Category) {
            
            $label = $Category->nom . ' ' . $Category->prenom . ' ' . $Category->level;
            
            $tree .='<li class="tree-view closed"<a class="tree-name">'. $label . '</a>';
            if(count($Category->childs)) {
                $tree .=$this->childView($Category);
            }
        }
        $tree .='<ul>';
        // return $tree;
        return view('files.treeview',compact('tree'));
    }
    public function childView($Category){                 
        $html ='<ul>';
        foreach ($Category->childs as $arr) {
            
            if(count($arr->childs)){
                
                $label = $Category->nom . ' ' . $Category->prenom . ' ' . $Category->level;
                
                $html .='<li class="tree-view closed"><a class="tree-name">'.$label.'</a>';  
                
                $html.= $this->childView($arr);
                
            }else{
                $html .='<li class="tree-view"><a class="tree-name">'.$label.'</a>';                                 
                $html .="</li>";
            }
            
        }
        
        $html .="</ul>";
        return $html;
    }
}
