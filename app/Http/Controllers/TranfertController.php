<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Transfert;
use App\Models\DemandeRetraits;
use App\Models\Transaction;
use App\Models\CompteCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TranfertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $i = 1;
        $tous = Transfert::where('user_id', Auth::user()->id)->get();
        return view('backend.transfert_list', compact('tous', 'i'));
    }
    public function create(){
    	return view('backend.transfert');
    }
    public function store(Request $req){

        $id = Auth::user()->id;
        $code = User::where('id', $id)->first();
        $nb_code = $code->nbr_code;
        /*$code = $code = CompteCode::where('user_id', $id)->orderBy('id', 'desc')->first();
        $nb_code = $code->nbr_code;*/
        $code_now = $nb_code - ($req->mont_transf);
    	$beneficiaire =  $req->receveur;
    	$recev =  User::where('pseudo', $beneficiaire)->first();
        $id_new = $recev->id;
        $solde_code = $recev->nbr_code;
        $new = $solde_code + ($req->mont_transf);

        if ($recev) {
            if($nb_code >= $req->mont_transf){
                Transfert::create([
                    'date_transf' => date("Y-m-d H:i:s"),
                    'user_id' => $id,
                    'motif' => $req->motif,
                    'mont_transf' => $req->mont_transf,
                    'receveur' => $req->receveur,
                ]);

                DB::table('users')
                        ->where('id', $id)
                        ->update(['nbr_code' => $code_now]);

                DB::table('users')
                        ->where('id', $id_new)
                        ->update(['nbr_code' => $new]);

                Transaction::create([
                    'date_transact' => date("Y-m-d H:i:s"),
                    'sender' => Auth::user()->pseudo,
                    'recever' => $req->receveur,
                    'motif' => "Transfert de code",
                    'mont_transact' => $req->mont_transf,
                ]);
            }return redirect()->route('home')->with('Enregistrement effectué');
            
        }
    }



    public function valider($id)
    {
        $dmde_id = $id;
        $solde_avt = DemandeRetraits::where('id', $dmde_id)->pluck('mont_dmde');
        $code = User::where('id', Auth::user()->id)->first();
        $nb_code = $code->solde;
        $new = $nb_code - ($solde_avt);
            
        DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['solde' => $new]);

        DB::table('demande_retraits')
            ->where('id', $dmde_id)
            ->update(['valide' => true]);

        return redirect()->route('demandes.index');
    }
}
