<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'pseudo' => 'required|string|unique:users',
            'email' => 'required|string|email|max:255',
            'pays' => 'required|string|max:50',
            'mdp_transact' => 'required|string|max:20',
            'no_compte' => 'required|string|max:25',
            'banque_id' => 'required|integer',
            'password' => 'required|string|min:6|confirmed',
            'pseudo_parrain' => 'required|string',
            'nom_payeur' => 'required|string'
        ];
    }
}
