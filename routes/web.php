<?php

use App\Data;
use App\Bonus;
use App\Upgrade;
use App\JOrgChart;
use App\MakeGenealogie;
use App\MakeGenealogieTwo;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('inscription', 'InscriptionController');
Route::resource('groupes_leaders', 'GroupeController');
Route::resource('demandes', 'DemandeController');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/connexion', 'HomeController@logout')->name('logout');
Route::get('/mon-profil', 'ProfilController@index')->name('myprofil');
Route::get('/profil/{var}', 'ProfilController@create')->name('profil');
Route::post('/profil-update/{var}', 'ProfilController@modif_profil')->name('profil.update');
Route::get('/mdp/{id}', 'ProfilController@modif_pwd')->name('user.pwd');
Route::post('/mdp-update/{id}', 'ProfilController@updatepassword')->name('pwd.update');

//Route::get('/profil/{id}', 'ProfilController@create')->name('profil.edit');
//Route::post('/user-update', 'ProfilController@store')->name('profil.update');
Route::get('recharge', 'RechargeController@create')->name('recharge.create');
Route::post('recharge', 'RechargeController@store')->name('recharge.store');
Route::get('generer', 'RechargeController@affich')->name('generer');
Route::post('generer', 'RechargeController@generer')->name('generer.store');
Route::get('transfert-list', 'TranfertController@index')->name('transfert.index');
Route::get('transfert', 'TranfertController@create')->name('transfert.create');
Route::post('transfert', 'TranfertController@store')->name('transfert.store');
Route::get(' valider_demande', 'TranfertController@valider')->name('valider');

// Route::get('category-tree-view',['uses'=>'CategoryController@manageCategory']);
Route::get('genealogie',['as'=>'genealogie','uses'=>'InscriptionController@manageCategory']);
Route::post('add-parrain',['as'=>'add.parrain','uses'=>'InscriptionController@addParrain']);
// Route::post('add-category',['as'=>'add.category','uses'=>'CategoryController@addCategory']);

Route::get('/proof', function () {
	$data = Data::all ();
    return view('proofs/tables')->withData ( $data );
});

Route::get('/upgrade/{id}', function ($id) {

    if($id != Auth::user()->id){
        $id = Auth::user()->id;
       return redirect()->route('upgrade', $id);
    }

    $upgrade = Upgrade::where('code', '=', $id)->get();
	return view('proofs/upgrade', compact('upgrade'));
})->name('upgrade');

Route::get('/addMembers', function () {
	return view('proofs/addMembers');
});

Route::post('memberscreate','MembersController@create');

Route::get('gene/{code}', function($code){

    $mkg = new MakeGenealogie($code);
    
    return view('proofs.gene')->with('tree', $mkg->treeView());

});

Route::get('/genetwo/{id}', function($id){


    if($id != Auth::user()->id){
        $id = Auth::user()->id;
       return redirect()->route('genealogie', $id);
    }

    $mkg = new MakeGenealogieTwo($id);
    
    return view('proofs.genetwo')->with('tree', $mkg->treeView());

})->name('genealogie');


