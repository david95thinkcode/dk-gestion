@extends('layouts.template')
@section('contenu')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Recharger un compte</h3>
      </div>
    </div>    
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <form class="form-horizontal" method="POST" action="{{route('recharge.store')}}">
                {{ csrf_field() }}

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Pseudo de l'utilisateur</label>
                    <input id="pseudo" type="text" class="form-control" name="pseudo" value="" required autofocus>
                    @if ($errors->has('pseudo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pseudo') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Nombre de codes à charger</label>
                    <input id="nbr_code" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"  class="form-control" name="nbr_code" value="" required autofocus>
                    @if ($errors->has('nbr_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nbr_code') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-primary" type="reset">Annuler</button>
                    <button type="submit" class="btn btn-success">Recharger</button>
                  </div>
                </div>
              </form>             

            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop