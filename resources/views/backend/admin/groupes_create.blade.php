@extends('layouts.template')
@section('contenu')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Enregistrement d'un Groupe Leader</h3>
      </div>
    </div>    
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <form class="form-horizontal" method="POST" action="{{route('groupes_leaders.store')}}">
                {{ csrf_field() }}

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Nom du groupe Leader</label>
                    <input id="nom_group" type="text" class="form-control" name="nom_group" value="" required autofocus>
                    @if ($errors->has('nom_group'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom_group') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Pays du siège</label>
                    <input id="pays" type="text" class="form-control" name="pays" value="" required autofocus>
                    @if ($errors->has('pays'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pays') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Responsable du groupe</label>
                    <input id="responsable" type="text" class="form-control" name="responsable" value="" required autofocus>
                    @if ($errors->has('responsable'))
                        <span class="help-block">
                            <strong>{{ $errors->first('responsable') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Contact</label>
                    <input id="contact" type="text" class="form-control" name="contact" value="" required autofocus>
                    @if ($errors->has('contact'))
                        <span class="help-block">
                            <strong>{{ $errors->first('contact') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-primary" type="reset">Annuler</button>
                    <button type="submit" class="btn btn-success">Sauvegarder</button>
                  </div>
                </div>
              </form>             

            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop