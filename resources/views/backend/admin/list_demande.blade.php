@extends('layouts.template')
@section('contenu')
  
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Commandes</h3>
              </div>              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste des demandes de retrait non validées </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="{{route('demandes.create')}}">
                        <button type="button" class="btn btn-primary">Nouveau</button>
                      </a>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>N°</th>
                          <th>Date de la demande </th>
                          <th>Nom demandeur</th>
                          <th>Montant demandé</th>
                          <th>Valider</th>                          
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($demandes as $dmde)
                        <tr>
                          <td>{{$i++}}</td>
                          <td>{{$dmde->date_dmde}}</td>
                          <td>{{$dmde->user_id}}</td>
                          <td>{{$dmde->mont_dmde}}</td>
                          <td><a href="{{route('valider', ['id'=>$dmde->id])}}"> <i class="fa fa-check-square-o"></i></a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@stop