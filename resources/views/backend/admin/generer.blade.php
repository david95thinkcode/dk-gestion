@extends('layouts.template')
@section('contenu')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Générer Code</h3>
      </div>
    </div>    
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <form class="form-horizontal" method="POST" action="{{route('generer.store')}}">
                {{ csrf_field() }}

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Nombre de codes </label>
                    <input id="nbr_code" type="number" min="1" class="form-control" name="nbr_code" value="" required autofocus>
                    @if ($errors->has('nbr_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nbr_code') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Generer</button>
                  </div>
                </div>
              </form>             

            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop