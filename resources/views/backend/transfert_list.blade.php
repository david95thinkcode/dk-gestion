@extends('layouts.template')
@section('contenu')
  
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Transferts</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste des transferts effectués </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="{{route('transfert.create')}}">
                        <button type="button" class="btn btn-primary">Nouveau</button>
                      </a>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>N°</th>
                          <th>Date transfert </th>
                          <th>Receveur</th>
                          <th>Motif</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($tous as $transf)
                        <tr>
                          <td>{{$i++}}</td>
                          <td>{{$transf->created_at}}</td>
                          <td>{{$transf->receveur}}</td>
                          <td>{{$transf->motif}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@stop