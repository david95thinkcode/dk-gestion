
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

   <!-- Bootstrap -->
    <link href="{{asset('theme/css/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('theme/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('theme/css/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('theme/css/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('theme/build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="login">
    <div>      
      <div class="col-md-8 center-margin">    
      <h3>Validation inscription</h3>   
      <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
        
        <h3>Payeur</h3>
        <input type="text" class="hidden"  name="user_id" value="{{ Auth::user()->id }}">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label>Nom du payeur</label>
            <input id="nom_payeur" type="text" class="form-control" name="nom_payeur" value="{{ old('nom_payeur') }}" required autofocus>
            @if ($errors->has('nom_payeur'))
                <span class="help-block">
                    <strong>{{ $errors->first('nom_payeur') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label>Mot de passe transaction </label>
            <input id="mdp_transact" type="mdp_transact" class="form-control" name="mdp_transact" value="{{ old('mdp_transact') }}" required autofocus>
            @if ($errors->has('mdp_transact'))
                <span class="help-block">
                    <strong>{{ $errors->first('mdp_transact') }}</strong>
                </span>
            @endif
          </div>
        </div> 

        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success">Valider</button>
          </div>
        </div>

      </form>
        
      </div>
    </div>
  </body>
</html>
