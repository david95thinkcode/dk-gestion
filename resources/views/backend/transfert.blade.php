@extends('layouts.template')
@section('contenu')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Transfert de code</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="x_content">
        <br />
          <form class="form-horizontal" method="POST" action="{{ route('transfert.store') }}">
            {{ csrf_field() }}
            
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group">
                <label>Pseudo du receveur </label>
                <input id="receveur" type="text" class="form-control" name="receveur" value="{{ old('receveur') }}" required autofocus>
                @if ($errors->has('receveur'))
                    <span class="help-block">
                        <strong>{{ $errors->first('receveur') }}</strong>
                    </span>
                @endif
              </div>
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group">
                <label>Motif du transfert </label>
                <input id="motif" type="text" class="form-control" name="motif" value="{{ old('motif') }}" required autofocus>
                @if ($errors->has('motif'))
                    <span class="help-block">
                        <strong>{{ $errors->first('motif') }}</strong>
                    </span>
                @endif
              </div>
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group">
                <label>Nombre de codes à transférer </label>
                <input id="mont_transf" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" name="mont_transf" value="{{ old('mont_transf') }}" required autofocus>
                @if ($errors->has('mont_transf'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mont_transf') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group">
                <label>Mot de passe transaction </label>
                <input id="mdp_transact" type="password" class="form-control" name="mdp_transact" value="{{ old('mdp_transact') }}" required autofocus>
                @if ($errors->has('mdp_transact'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mdp_transact') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="reset">Annuler</button>
                <button type="submit" class="btn btn-success">Sauvegarder</button>
              </div>
            </div>   
          </form> 
      </div>
    </div>
  </div>
@stop