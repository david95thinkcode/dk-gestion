@extends('layouts.template')
@section('contenu')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Profil</h3>
      </div>
    </div>    
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <form class="form-horizontal" method="POST" action="{{ route('pwd.update', Auth::user()->id) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}                

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Ancien mot de passe </label>
                    <input id="mdp_ancien" type="password" class="form-control" name="mdp_ancien" value="{{ old('mdp_ancien') }}" required autofocus>
                    @if ($errors->has('mdp_ancien'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mdp_ancien') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label>Nouveau mot de passe </label>
                    <input id="mdp_transact" type="password" class="form-control" name="mdp_transact" value="{{ old('mdp_transact') }}" required autofocus>
                    @if ($errors->has('mdp_transact'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mdp_transact') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label>Confirmer mot de passe</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                  </div>
                </div>    
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-primary" type="reset">Annuler</button>
                    <button type="submit" class="btn btn-success">Sauvegarder</button>
                  </div>
                </div>
              </form>             

            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop