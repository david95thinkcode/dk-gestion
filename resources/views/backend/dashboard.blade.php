@extends('layouts.template')
@section('contenu')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats" style="background-color: violet;">
          <div class="icon"><i class="fa fa-money"></i></div>
          <div class="count">{{$solde}}</div>
          <h3> FCFA</h3>
          <p>Solde</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats" style="background-color: #006600;">
          <div class="icon"><i class="fa fa-comments-o"></i></div>
          <div class="count">{{$code}}</div>
          <h3>Enregistrements</h3>
          <p>Codes</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats" style="background-color: #660066;">
          <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          <div class="count">{{ $lev }}</div>
          <h3>Niveau</h3>
          <p>Niveaux </p>
        </div>
      </div>
    </div>
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="tile-stats" style="background-color: #0000e6;">
          <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
          <div class="count">{{ $kids }}</div>
          <h3>Nombre de fileuls</h3>
        </div>
      </div>
      <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="tile-stats" style="background-color: #e62e00;">
          <div class="icon"><i class="fa fa-comments-o"></i></div>
          <div class="count">{{ $bonus }}</div>
          <h3>Bonus</h3>
        </div>
      </div>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Toutes les transactions </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li><!-- 
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li> 
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>-->
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Date transaction</th>
                  <th>Envoyeur</th>
                  <th>Receveur</th>
                  <th>Motif</th>
                  <th>Montant</th>
                </tr>
              </thead>


              <tbody>
                @foreach($transactions as $transact)
                <tr>
                  <td>{{$transact->date_transact}}</td>
                  <td>{{$transact->sender}}</td>
                  <td>{{$transact->recever}}</td>
                  <td>{{$transact->motif}}</td>
                  <td>{{$transact->mont_transact}}</td>
                </tr>
                @endforeach
                                
              </tbody>
            </table>
          </div>
        </div>
      </div>    
    </div>
  </div>
</div>
@stop