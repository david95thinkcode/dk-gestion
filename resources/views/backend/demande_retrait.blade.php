@extends('layouts.template')
@section('contenu')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Demande de retrait </h3>
      </div>
    </div>    
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <form class="form-horizontal" method="POST" action="{{route('demandes.store')}}">
                {{ csrf_field() }}

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Date de la demande</label>
                    @if ($errors->has('date_dmde'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date_dmde') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Pays habité</label>
                    <input id="pays" type="text" class="form-control" name="pays" value="" required autofocus>
                    @if ($errors->has('pays'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pays') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Votre groupe leader</label>
                    <select class="form-control" name="group_id" required>
                      <option></option>
                      @foreach($groupes as $group)
                      <option value="{{$group->id}}"> {{$group->nom_group}} </option>
                      @endforeach
                    </select>
                  </div>
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Montant à retirer</label>
                    <input id="mont_dmde" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" name="mont_dmde" value="" required autofocus>
                    @if ($errors->has('mont_dmde'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mont_dmde') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-primary" type="reset">Annuler</button>
                    <button type="submit" class="btn btn-success">Sauvegarder</button>
                  </div>
                </div>
              </form>             

            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop