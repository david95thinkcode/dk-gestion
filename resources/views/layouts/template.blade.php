
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DK Business </title>

    <!-- Bootstrap -->
    <link href="{{asset('theme/css/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('theme/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('theme/css/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('theme/css/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

     <link href="{{asset('theme/css/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('theme/css/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="{{asset('theme/build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- sidebar -->
        @include('partials.sidebar')
        
        <!-- top navigation -->
        @yield('contenu')
        <!-- /top navigation -->

        <!-- page content -->
        
        <!-- /page content -->

        <!-- footer content -->
        @include('partials.footer')
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('theme/css/jquery/dist/jquery.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('theme/css/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('theme/css/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('theme/css/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('theme/css/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{asset('theme/css/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('theme/css/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('theme/css/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('theme/css/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('theme/css/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('theme/css/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{asset('theme/css/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('theme/css/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('theme/css/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{asset('theme/css/DateJS/build/date.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('theme/css/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('theme/css/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('theme/css/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('theme/css/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{asset('theme/build/js/custom.min.js')}}"></script>
  </body>
</html>