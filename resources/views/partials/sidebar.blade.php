<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{ url('/') }}" class="site_title"><img src="{{asset('theme/images/DK1.jpg')}} " style="width: 35px; height: 35px;"> <span>DK Business</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_info">
        <span>Bienvenue,</span>
        <h2>{{ Auth::user()->first_name }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
          <li><a href="{{route('home')}} "><i class="fa fa-home"></i> Tableau de bord </a></li>
          <li><a href=""><i class="fa fa-home"></i> New Deposit </a></li>
          <li><a><i class="fa fa-edit"></i> Portefeuille <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{route('transfert.create')}}"> Transfert de code</a></li>
              <li><a href="{{route('demandes.create')}}">Demande de Retrait</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-desktop"></i> Historiques <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{route('transfert.index')}}">Transferts</a></li>
              <li><a href="">Mon Solde</a></li>
              <li><a href="">Commission</a></li><!-- 
              <li><a href="">Wihdrawal</a></li> -->
            </ul>
          </li>
          <li><a href="{{ route('genealogie', ['id' => Auth::user()->id])}}"><i class="fa fa-sitemap"></i> Généalogie </a></li>
          <li><a href="{{ route('upgrade', ['id' => Auth::user()->id])}}"><i class="fa fa-chevron-up" ></i> Niveau </a></li> 
          <li><a href="{{ url('proof')  }}"><i class="fa fa-bars" ></i> Liste </a></li> 
          <li><a href="{{route('inscription.create')}} "><i class="fa fa-home"></i> Ajouter un membre</a></li>
          @if(Auth::user()->role == 'administrateur')
          <li><a href="{{route('groupes_leaders.index')}} "><i class="fa fa-home"></i> Groupes Leaders</a></li>
          <li><a href="{{route('demandes.index')}} "><i class="fa fa-home"></i> Liste des Demandes</a></li>
          <li><a href="{{route('generer')}} "><i class="fa fa-home"></i> Générer code</a></li>
          <li><a href="{{route('recharge.create')}} "><i class="fa fa-home"></i> Recharge Compte</a></li>
          @endif
          <!-- <li><a><i class="fa fa-bar-chart-o"></i> Niveaux <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="">Niveau 1</a></li>
              <li><a href="">Niveau 2</a></li>
              <li><a href="">Niveau 3</a></li>
              <li><a href="">Niveau 4</a></li>
              <li><a href="">Niveau 5</a></li>
              <li><a href="">Niveau 6</a></li>
            </ul>
          </li> -->
        </ul>
      </div>
      <!-- <div class="menu_section">
        <h3>Settings</h3>
        <ul class="nav side-menu">
          <li><a><i class="fa fa-windows"></i> Mon Compte <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="">Profil</a></li>
              <li><a href="">Mot de passe transactions</a></li>
              <li><a href="">Plain Page</a></li>
            </ul>
          </li>
        </ul>
      </div> -->

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">

    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
<!-- top nav -->

<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ Auth::user()->first_name }}
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="{{ route('myprofil') }}"> Profil</a></li>
            <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out pull-right"></i> Déconnexion</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>