
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages:["orgchart"]});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Name');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'ToolTip');

            data.addRows(<?=$tree?>);
            
            var options = {
                title : "Genealogie",
                allowHtml:true
            }
            
            // Create the chart.
            var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
            // Draw the chart, setting the allowHtml option to true for the tooltips.
            chart.draw(data, options);
        }
    </script>

    <title>DK Business | Genealogie</title>

    <!-- Bootstrap -->
    <!-- Font Awesome -->
    <link href="{{ asset('theme/css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('theme/build/css/custom.min.css') }}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- sidebar -->
        <div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{ url('/')  }}" class="site_title"><img src="{{ asset('theme/images/DK1.jpg') }} " style="width: 35px; height: 35px;"> <span>DK Business</span></a>
    </div>

    <div class="clearfix"></div>
    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
          <li><a href="{{ route('upgrade', ['id'=> Auth::user()->id]) }}"><i class="fa fa-chevron-up"></i> Niveaux</a></li>

        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

  </div>
</div>
<!-- top nav -->

<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        
      </ul>
    </nav>
  </div>
</div>        
        <!-- top navigation -->
        <div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles">
      
        <!-- page content -->
        <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                </br>
                <button type="button" style="background-color:#FFFFFF">Membre
                </button>
                <button type="button" style="background-color:#F1C40F">Niveau 1
                </button>
                <button type="button" style="background-color:#E74C3C">Niveau 2
                </button>
                <button type="button" style="background-color:#C0392B">Niveau 3
                </button>
                <button type="button" style="background-color:#27AE60">Niveau 4
                </button>
                <button type="button" style="background-color:#3498DB">Niveau 5
                </button>
                <button type="button" style="background-color:#95A5A6">Niveau 6
                </button>
                <button type="button" style="background-color:#1ABC9C">Diamant
                </button>
              </div></br><div id="chart_div"></div>
            </div>
          </div>
        <!-- page content -->

  </div>
</div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('theme/css/jquery/dist/jquery.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('theme/css/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('theme/build/js/custom.min.js') }}"></script>
  </body>
</html>