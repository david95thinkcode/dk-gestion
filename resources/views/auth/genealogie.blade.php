<ul>
@foreach($childs as $child)
	<li>
	    {{ $child->pseudo_fils }}
		@if(count($child->childs))
            @include('auth.genealogie',['childs' => $child->childs])
        @endif
	</li>
@endforeach
</ul>