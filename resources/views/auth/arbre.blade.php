<!DOCTYPE html>
<html>
<head>
  <pseudo_fils>Laravel Category Treeview Example</pseudo_fils>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="/css/treeview.css" rel="stylesheet">
</head>
<body>
  <div class="container">     
    <div class="panel panel-primary">
      <div class="panel-heading">Manage Category TreeView</div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <h3>Category List</h3>
                <ul id="tree1">
                    @foreach($categories as $category)
                        <li>
                            {{ $category->pseudo_fils }}
                            @if(count($category->childs))
                                @include('auth.genealogie',['childs' => $category->childs])
                            @endif
                        </li>
                    @endforeach

                </ul>
            </div>

            <!-- <div class="col-md-6">
              <h3>Add New Category</h3>


              <form class="form-horizontal" method="POST" action="{{ route('add.parrain') }}">
                        {{ csrf_field() }}
                        @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                        @endif
                        <div class="form-group{{ $errors->has('pseudo_fils') ? ' has-error' : '' }}">
                            <label for="pseudo_fils" class="col-md-4 control-label">pseudo_fils</label>

                            <div class="col-md-6">
                                <input id="pseudo_fils" type="text" class="form-control" name="pseudo_fils" value="{{ old('pseudo_fils') }}" required autofocus>

                                @if ($errors->has('pseudo_fils'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pseudo_fils') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                            <label for="parent_id" class="col-md-4 control-label">parent</label>

                            <div class="col-md-6">
                                <select class="form-control" name="parrain_id">
                                  @foreach($allCategories as $banq)
                                  <option value="{{$banq->id}}"> {{$banq->pseudo_fils}} </option>
                                  @endforeach
                                </select>
                                @if ($errors->has('parent_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add New
                                </button>
                            </div>
                        </div>
                    </form>               

            </div> -->
          </div>        
        </div>
        </div>
    </div>
    <script src="/js/treeview.js"></script>
</body>
</html>

