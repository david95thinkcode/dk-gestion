
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DK Business </title>

   <!-- Bootstrap -->
    <link href="{{asset('theme/css/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('theme/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('theme/css/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('theme/css/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('theme/build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="login">
    <div>      
      <div class="col-md-8 center-margin">
        <br>
        <a class="btn btn-information" href="/home" style="background-color:greenyellow">
          Retourner a l'accueil
        </a>

      <h3>Inscription</h3>

      <div class="row">
          <div class="row col-md-12 col-sm-12 col-xs-12">
              <br> <br>
              @if (Session::has('errorMessage'))
                  <div class="panel panel-warning">
                      <div class="panel-heading">
                          <h3 class="panel-title">Impossible de vous inscrire</h3>
                        </div>
                    <div class="panel-body">
                      {{ Session::get("errorMessage") }}
                    </div>
                  </div>
                  @endif
              </div>
      </div>

      <div class="row">
        <form class="form-horizontal" method="POST" action="{{ route('inscription.store') }}">
                        {{ csrf_field() }}
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Prénom </label>
            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
            @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Nom</label>
            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Pseudo </label>
            <input id="pseudo" type="text" class="form-control" name="pseudo" value="{{ old('pseudo') }}" required autofocus>
            @if ($errors->has('pseudo'))
                <span class="help-block">
                    <strong>{{ $errors->first('pseudo') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Email</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Telephone </label>
            <input id="tel" type="text" class="form-control" name="tel" value="{{ old('tel') }}" required autofocus>
            @if ($errors->has('tel'))
                <span class="help-block">
                    <strong>{{ $errors->first('tel') }}</strong>
                </span>
            @endif
          </div>
        </div> -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Pays</label>
            <input id="pays" type="text" class="form-control" name="pays" value="{{ old('pays') }}" required autofocus>
            @if ($errors->has('pays'))
                <span class="help-block">
                    <strong>{{ $errors->first('pays') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Mot de passe transactions </label>
            <input id="mdp_transact" type="password" class="form-control" name="mdp_transact" value="<?php echo str_random(10);?>" required autofocus>
            @if ($errors->has('mdp_transact'))
                <span class="help-block">
                    <strong>{{ $errors->first('mdp_transact') }}</strong>
                </span>
            @endif
          </div>  
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label>Parrain</label>
            <input id="pseudo_parrain" type="text" class="form-control" name="pseudo_parrain" value="{{ old('pseudo_parrain') }}" required autofocus>
            @if ($errors->has('pseudo_parrain'))
                <span class="help-block">
                    <strong>{{ $errors->first('pseudo_parrain') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Mot de passe </label>
            <input id="password" type="password" class="form-control" name="password" required autofocus>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Confirmer mot de passe</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
          </div>
        </div>   

        <h3>Informations bancaires</h3>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Numero de compte </label>
            <input id="no_compte" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" name="no_compte" value="{{ old('no_compte') }}" required autofocus>
            @if ($errors->has('no_compte'))
                <span class="help-block">
                    <strong>{{ $errors->first('no_compte') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Banque</label>
            <select class="form-control" name="banque_id">
              <option></option>
              @foreach($all_banques as $banq)
              <option value="{{$banq->id}}"> {{$banq->nom_banq}} </option>
              @endforeach
            </select>
          </div>
        </div>  

        <h3>Payeur</h3>
        <input type="text" class="hidden"  name="">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Nom du payeur</label>
            <input id="nom_payeur" type="text" class="form-control" name="nom_payeur" value="{{ old('nom_payeur') }}" required autofocus>
            @if ($errors->has('nom_payeur'))
                <span class="help-block">
                    <strong>{{ $errors->first('nom_payeur') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <label>Mot de passe transaction </label>
            <input id="mdp" type="password" class="form-control" name="mdp" value="" required autofocus>
            @if ($errors->has('mdp'))
                <span class="help-block">
                    <strong>{{ $errors->first('mdp') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button class="btn btn-primary" type="reset">Annuler</button>
            <button type="submit" class="btn btn-success">Enregistrer</button>
          </div>
        </div>

      </form>
      </div>
      
        
      </div>
    </div>
  </body>
</html>
