
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>DK Business</title>

  <!-- Bootstrap -->
  <link href="{{asset('page/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('page/css/font-awesome.min.css')}}">
  <link href="{{asset('page/css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('page/css/prettyPhoto.css')}}" rel="stylesheet">
  <link href="{{asset('page/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('page/css/responsive.css')}}" rel="stylesheet">
  

</head>

<body class="homepage">
  <header id="header">
    <nav class="navbar navbar-fixed-top" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">DK Business</a>
        </div>

        <div class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                @if (Route::has('login'))
            <li class="active"><a href="">Accueil</a></li>
            @auth
                        <li><a href="{{ url('/home') }}">Dashboard</a></li>
                    @else
            <li ><a href="{{ route('login') }}">Se connecter</a></li>
            <li><a href="{{ route('register') }}">Inscription</a></li>
            @endauth
            @endif
          </ul>
        </div>
      </div>
      <!--/.container-->
    </nav>
    <!--/nav-->

  </header>
  <!--/header-->

  <div class="slider">
    <div class="container">
      <div id="about-slider">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators visible-xs">
            <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-slider" data-slide-to="1"></li>
            <li data-target="#carousel-slider" data-slide-to="2"></li>
          </ol>

          <div class="carousel-inner">
            <div class="item active">
              <img src="{{asset('page/images/slider_one.jpg')}}" class="img-responsive" alt="">
            </div>
            <div class="item">
              <img src="{{asset('page/images/slider_one.jpg')}}" class="img-responsive" alt="">
            </div>
            <div class="item">
              <img src="{{asset('page/images/slider_one.jpg')}}" class="img-responsive" alt="">
            </div>
          </div>

          <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>

          <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
        </div>
        <!--/#carousel-slider-->
      </div>
      <!--/#about-slider-->
    </div>
  </div>

  <section id="feature">
    <div class="container">
      <div class="center wow fadeInDown">
        <h2>Pourquoi DK Business?</h2>        
      </div>

      <div class="row">
        <div class="features">
          <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">
              <h2 style="text-align: center; font-size: 30px; color: yellow;">MISSION</h2>
              <p>
                Offrir des opportunités de développements aux jeunes africains qui
                s’aventureront dans le marketing relationnel, grâce au financement
                participatif qui leur permettra de créer de la valeur ajoutée
              </p>
            </div>
          </div>
          <!--/.col-md-4-->

          <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">
              <h2 style="text-align: center; font-size: 30px; color: yellow;">VISION</h2>
              <p>
                Etre le principal réseau panafricain de Jeunes citoyens épanouis
innovants grâce à la formation ,le leadership, et l'entreprenariat
              </p>
            </div>
          </div>
          <!--/.col-md-4-->

          <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">
              <h2 style="text-align: center; font-size: 30px; color: yellow;">OBJECTIF</h2>
              <p>
                Investir dans l'humain par notre mission et notre vision, Réduire le taux de
chômage et le sous emploi grâce au financement et aux dons du membre
individuel. Améliorer le quotidien de tous nos membres
              </p>
            </div>
          </div>
        </div>
        <br>
        <br>
        <!--/.services-->
      </div>
      <!--/.row-->
    </div>
    <!--/.container-->
  </section>
  <!--/#feature-->

  <section id="middle">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 wow fadeInDown">
          <div class="skill">
            <h2>Comment devenir membre?</h2>
            <ul>
              <li>Accepter les termes et conditions de la compagnie</li>
              <li>Partager la même vision que tous les membres</li>
              <li>Une inscription unique dont la valeur est de 5000F</li>
              <li>Parrainer obligatoirement au moins deux personnes</li>
              <li>Etre un bon leader, dynamique et actif</li>
            </ul>
          </div>

        </div>
        <!--/.col-sm-6-->

        <div class="col-sm-6 wow fadeInDown">
          <div class="accordion">
            <h2>Présentation de la matrice</h2>
            <ul>
              <li>Très loin des matrices classiques et empiriques</li>
              <li>Des payements de bonus juste après deux parrainages</li>
              <li>Matrice binaire, souple et flexible</li>
              <li>Des changeles trimestriels</li>
              <li>Des payements hebdomadaires</li>
              <li>Sécurisé et très fiable</li>
             </ul>
               
            </div>
            <!--/#accordion1-->
          </div>
        </div>

      </div>
      <!--/.row-->
    </div>
    <!--/.container-->
  </section>
  <!--/#middle-->
  <footer id="footer" class="midnight-blue">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          &copy; DK business. All Rights Reserved.
         
        </div>
        <div class="col-sm-6">
          <ul class="pull-right">
            <li><a href="#">Accueil</a></li>
            <li><a href="#">A Propos</a></li>
            <li><a href="#">Faq</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!--/#footer-->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="{{asset('page/js/jquery.js')}}"></script>
  <script src="{{asset('page/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('page/js/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('page/js/wow.min.js')}}"></script>
  <script src="{{asset('page/js/main.js')}}"></script>

</body>

</html>
