<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('pseudo')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('pays');
            $table->string('level')->default('Membre');
            $table->string('mdp_transact');
            $table->string('no_compte');
            $table->string('nbr_code')->default(0);
            $table->string('solde')->default(0);
            $table->integer('banque_id')->unsigned()->nullable();
            $table->foreign('banque_id')->references('id')->on('banques');
            $table->string('password');
            $table->string('parrain_id');
            $table->string('role')->default('utilisateur');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
