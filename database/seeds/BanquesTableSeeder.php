<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BanquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banques')->insert([
            'nom_banq' => 'BOA Group',
			'adresse' => 'Cote d Ivoire',
        ]);

        DB::table('banques')->insert([
            'nom_banq' => 'ECOBANK',
			'adresse' => 'Cotonou Benin',
        ]);

        DB::table('banques')->insert([
            'nom_banq' => 'NSIA Bank',
			'adresse' => 'Cotonou Benin',
        ]);

        DB::table('banques')->insert([
            'nom_banq' => 'SGBE',
			'adresse' => 'Cotonou Benin',
        ]);
    }
}
