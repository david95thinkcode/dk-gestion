<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompteSoldeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compte_soldes')->insert([
            'date_solde' => date("Y-m-d H:i:s"),
			'user_id' => 1,
			'solde' => 100000,
        ]);
    }
}
