<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompteCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compte_codes')->insert([
            'date_solde' => date("Y-m-d H:i:s"),
			'user_id' => 1,
			'nbr_code' => 1000,
        ]);
    }
}
