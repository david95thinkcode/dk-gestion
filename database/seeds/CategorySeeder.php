<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'Premier',
			'parent_id' => 0,
        ]);
        DB::table('categories')->insert([
            'title' => 'Deuxieme',
            'parent_id' => 1,
        ]);
    }
}
