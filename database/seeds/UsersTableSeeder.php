<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'pseudo' => 'admin',
			'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'admin@gmail.com',
            'pays' => 'Nigeria',
            'mdp_transact' => str_random(10),
            'no_compte' => '1234567890',
            'nbr_code' => '1000',
            'solde' => '1000000',
            'password' => bcrypt('@dmin12345'),
            'role' => 'administrateur',
            'level' => 'Membre',
            'parrain_id' =>'',
        ]);
        DB::table('users')->insert([
            'pseudo' => 'Coqnet',
            'first_name' => 'Arnold',
            'last_name' => 'Sossa',
            'email' => 'edemsossa@gmail.com',
            'pays' => 'Bénin',
            'mdp_transact' => str_random(10),
            'no_compte' => '3456789012',
            'nbr_code' => '100',
            'solde' => '0',
            'password' => bcrypt('Change123'),
            'role' => 'utilisateur',
            'level' => 'Membre',
            'parrain_id' =>1,
        ]);
         DB::table('parrains')->insert([
            'pseudo_fils' => 'admin',
            'parrain_id' => 0,
        ]);

        DB::table('parrains')->insert([
            'pseudo_fils' => 'Coqnet',
            'parrain_id' => 1,
        ]);

    }
}
